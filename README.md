#atdiff 

Helps you determine whether or not a website has been modified during a migration, so you can re-rsync htdocs/db, et cetera. **Please note that only public end-user visible areas of the site are able to be analyzed.**

###Dependencies
* python-dnspython

###TODOs:
1. Implement option to compare a list of sites in a file. use : `wget --input-file=file`
1. Implement option to check file timestamps instead of contents. This should reduce runtime slightly.
