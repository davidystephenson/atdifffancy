#!/usr/bin/env python
# Written May 2015 by Erik Natvig {enatvig[at]fedoraproject.org} -- Last updated 2015-5-09
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os sys dns.resolver argparse

def main():
 
    parser = argparse.ArgumentParser(__file__, description="Determines differences between live and migrated versions of a website")
    parser.add_argument("--list", "-l", help="Check multiple sites listed within a file.", action="store_true") # Not ready
    parser.add_argument("--time", "-t", help="Check file timestamps instead of contents. Faster.", action="store_true") # Not ready
    parser.add_argument("host", help="The website to check", type=str)
    parser.add_argument("ns", help="The new name server", type=str)

    args = parser.parse_args()

    r = dns.resolver.Resolver()
    r.namerservers = [new_ns]
    args.ns = r.query(args.host, 'A').response # Acquire IP address of migrated non-live site from new name server.

    os.mkdir('%s') % args.host
    os.makedirs('b/%s') % args.host
    os.system('wget --recursive %s %s/' % (args.host,args.host))  # Download live web site.
    remove_hosts_entry(args.host)                                 # Remove any existing /etc/hosts entry.
    os.system('echo "%s %s" >> /etc/hosts' % (args.host,args.ns)) # Create /etc/hosts entry.
    os.system('wget --recursive %s b/%s' % (args.host,args.host)  # Download migrated non-live web site.
    remove_hosts_entry(args.host)                                 # Remove /etc/hosts entry.
    os.system('clear')
    os.system('diff -qr %s/ b/%s/ > atdiff.txt') % (args.host,args.host) # Compare the site files for differences and write any to file.
    os.system('rm -rf %s b/%s b/') % (args.host,args.host)               # Remove the downloaded site files.
    os.system('cat atdiff.txt | less')                                   # Display any differences.

def remove_hosts_entry(args.host):
    f = open("/etc/hosts","r+")
    d = f.readlines()
    f.seek(0)
    for i in d:
        if i != args.host:
            f.write(i)
    f.truncate()
    f.close()
    
if __name__ == "__main__":
    main()